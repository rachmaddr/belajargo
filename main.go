package main

import (
	"fmt"
	"strconv"
)

func main() {
	var x int
	var y int
	var prime int

	fmt.Println("Masukan vairable x :")
	fmt.Scan(&x)

	fmt.Println("Masukan vairable y :")
	fmt.Scan(&y)

	totalPenjumlahan := Penjumlahan(x, y)

	var hasilPenjumlahan string = strconv.Itoa(totalPenjumlahan)
	fmt.Println("Penjumlahan x dan y : " + hasilPenjumlahan)

	totalPerkalian := Perkalian(x, y)
	fmt.Print("Pekalian x dan y : ", totalPerkalian)

	fmt.Println("\nMasukan banyaknya bilangan prima dan fibonacci :")
	fmt.Scan(&prime)

	primes := BilanganPrima(prime)
	fmt.Println("Bilangan Prima : " + primes)

	fibonacci := DeretFibonacci(prime)
	fmt.Println("Deret Fibonacci : " + fibonacci)
}

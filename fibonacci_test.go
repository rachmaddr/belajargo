package main

import (
	"fmt"
	"strings"
	"testing"
)

func TestFibonaccy(t *testing.T) {
	fibonacciSequence := DeretFibonacci(13)

	if strings.Compare(fibonacciSequence, "0,1,1,2,3,5,8,13,21,34,55,89,144") != 0 {
		t.Errorf("Fibonacci was incorrect, got: %s, want: 2,3,5,7,11,13,17,19,23,29,31,37,41.",
			fibonacciSequence)
	}

	fmt.Println("Deret Fibonacci sebanyak 13 = " + fibonacciSequence)
}

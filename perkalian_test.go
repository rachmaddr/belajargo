package main

import (
	"fmt"
	"strconv"
	"testing"
)

func TestMultiple(t *testing.T) {
	total := Perkalian(5, 5)
	if total != 25 {
		t.Errorf("Multiple was incorrect, got: %d, want: %d.", total, 10)
	}

	stringTotal := strconv.Itoa(total)
	fmt.Println("Perkalian dari 5 x 5 = " + stringTotal)
}

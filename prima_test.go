package main

import (
	"fmt"
	"strings"
	"testing"
)

func TestPrime(t *testing.T) {
	primes := BilanganPrima(13)

	if strings.Compare(primes, "2,3,5,7,11,13,17,19,23,29,31,37,41") != 0 {
		t.Errorf("Prime was incorrect, got: %s, want: 2,3,5,7,11,13,17,19,23,29,31,37,41.", primes)
	}

	fmt.Println("Bilangan prima sebanyak 13 = " + primes)
}

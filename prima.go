package main

import "strconv"

func BilanganPrima(n int) string {

	var primeN string = "2"
	counter := 2
	counterPrime := 1

	for counterPrime < n {
		counter++

		if CheckPrime(counter) {
			primeN = primeN + ","
			primeN = primeN + strconv.Itoa(counter)
			counterPrime++
		}
	}

	return primeN
}

func CheckPrime(prime int) bool {
	for i := 2; i < prime; i++ {
		if prime%i == 0 {
			return false
		}
	}

	return true
}

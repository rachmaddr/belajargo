package main

import "strconv"

func DeretFibonacci(n int) string {
	var fibonacciSequence string

	tempNumber1 := 0
	tempNumber2 := 1
	nextNumber := 0

	for i := 1; i <= n; i++ {
		if i == 1 {
			fibonacciSequence = strconv.Itoa(tempNumber1)
			continue
		}
		if i == 2 {
			fibonacciSequence = fibonacciSequence + "," + strconv.Itoa(tempNumber2)
			continue
		}
		nextNumber = tempNumber1 + tempNumber2
		tempNumber1 = tempNumber2
		tempNumber2 = nextNumber
		fibonacciSequence = fibonacciSequence + "," + strconv.Itoa(nextNumber)
	}

	return fibonacciSequence
}

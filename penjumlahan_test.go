package main

import (
	"fmt"
	"strconv"
	"testing"
)

func TestSum(t *testing.T) {
	total := Penjumlahan(5, 5)
	if total != 10 {
		t.Errorf("Sum was incorrect, got: %d, want: %d.", total, 10)
	}

	stringTotal := strconv.Itoa(total)
	fmt.Println("Jumlah dari 5 + 5 = " + stringTotal)
}
